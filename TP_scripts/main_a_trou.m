%% Real test, learning from other images

clear all
close all
clc

%% Parameters
k = ; 
dims = ;
skip = ;
fact = ; % conseil: fact = 1

%% importing images, building the training data matrix

% Imports
Blanquer = ;
Castaner = ;
Darmanin = ;
Parly    = ;
% aide : faire help imread

% Patches
Y1 = ;
Y2 = ;
Y3 = ;
Y4 = ;
% aide : regarder dans les fonctions fournies...

% Building the training data
Y = [Y1,Y2,Y3,Y4];

%% Training the dictionary

% Sparse coding steps
[Ddl, Xdl, e, ~, ~, ~] = ;

% Reconstructed patches of Parly
[d,n] = size(Xdl);
Y4_est = Ddl*Xdl(:,n*3/4+1:end);

% Reconstructed Parly using patch2image (Aide : regarder dans les
% fonctions fournies)
Parly_est = ;


% Plot the true Parly and the reconstructed one, compare visually


%% Estimating the missing pixels in Edouard

close all

% Loading the clean image and mask
Edouard = ;
mask_im = imread('faces_gouv/mask.png');

%------------ provided for free --------------%
% Infering the mask of corrupted pixels (given to the students)
mask = (mask_im > 250);
mask_inv = (mask_im <= 250);
% Corrupting the image
Edouard_tag = Edouard;
Edouard_tag(mask_inv) = 0;
%----------------------------------------------%

% Showing the corrurpted image

% Showing the mask

% Building patch matrix of corrupted Edouard and mask
    % note: for the matrix of mask, use logical() on the output of the
    % patch function

% Sparse coding of corrupted image (it has missing entries!)
k2 = ; %more precise fitting here if possible
[Xcorr,ecorr] = ;
% Normalisation of the error
ecorr = ;

% Reconstuction

% Did we remove the text ? Show the reconstructed image and be baffled! (or not)
