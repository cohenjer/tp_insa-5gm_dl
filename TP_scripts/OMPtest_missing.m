% Testing the OMP function

clear variables
close all
clc

% Parameters
m = 20; 
k = 3;
d = 30;
n = 4;

% Dictionary
    % génération aléatoire
D = randn(m,d);
    % normalisation
D = D.*repmat(1./sqrt(sum(D.^2)),m,1);

% Sparse true coefficients and data point
x = zeros(d,n);
x(1:k,:) = randn(k,n);
y = D*x;

% Random mask
mask = randn(m,n);
mask = mask<0.9;

% Reconstruction of x
[x_est,ey] = OMP_missing(y,D,k,mask);

% error
errx = norm(x - x_est,'fro')^2
ey

% Note that if you repeat the experiments several times, it sometimes fails
% and sometimes works.