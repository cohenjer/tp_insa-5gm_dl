function Y = patches (myImage, dims, skip)
%% Help of patches
% Y = patches (image, dimensions, skip)
% This function computes a patch matrix Y from image, which is a (RGB) color
% image stored in a 3-way array. The user can specify the size of the patches
% dims (the third dimensions has to be 3), and the
% overlap of patches by specifying the amount of shift pixelwise between each
% patch.
%-----------------
% inputs:
%
% image : a tensor containing the image. May be a mask of missing data.
% dims : dimensions of the patches. Example: [10,10,3]. Third dimensions has to
% be 3 (this function is designed for RGB images or masks of missing data for
% RGB images).
%
% outputs:
%
% Y : matrix containing vectorized patches column-wise. Stored in double
% format.
%
%----------------
% Credits to Jeremy Cohen. This function is designed for educational
% purpose. Made for INSA Dictionary Learning course.
%---------------------------------------------------------------------------

% Sanity check
if dims(3) ~= 3
    fprintf('dims(3) has to be 3')
    return
end

% Parameters
[m1,m2,~] = size(myImage);

% Initialisation of Y
Y = [];

% Initialisation of counters
x = 1;
y = 1;

% Building the patches
while x+dims(1)-1<=m1
    while y+dims(2)-1<=m2                
        impatch = myImage(x:(x+dims(1)-1),y:(y+dims(2)-1),1:dims(3));
        Y = [Y, impatch(:)];
        y = y + skip;
    end
    x = x + skip;
    y = 1;
end
Y =double(Y);
