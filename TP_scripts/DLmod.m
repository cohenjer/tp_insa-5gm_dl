function [D,X,e,Do,Xo,ei] = DLmod (Y,k,dims,fact)
%% help of DLmod
% [D,X,e,Do,Xo,ei] = DLmod(Y,k)
% This function computes a sparsity inducing dictionary D and a matrix of
% sparse coefficients X using the data stored in Y and a sparsity level k.

% The algorithm implemented is Method of Optimal Directions (MOD), which
% alternates between
%
%   D = argmin_V \|Y - VX \|_F^2;  \|d_j\|_2 = 1
%
% which is a quadratic problem, and
%
%   X = argmin_B \| Y - DX \|_F^2; \|x_i\|_0 < k+1
%
% which is solved by OMP as a sparse coding problem.
% A mask is allowed to account for missing data on Y.
%
% Initialisation is carried out using a truncated DCT dictionnary, and the
% corresponding k-sparse coefficients.
%
%-----------------------------
% Inputs:
% 
% Y : matrix of data 
% k : sparsity level
% dims : size of the patches
% fact : relative size of dictionary with respect to patch sizes. Final number
% of atoms is fact^3 * prod(dims)
% 
% Outputs:
%
% D : output learnt dictionary
% X : sparse matrix of coefficients
% e : reconstruction error
% Do : initial DCT dictionary (baseline)
% Xo : initial k-sparse coefficients for the DCT dictionary (baseline)
% ei : relative reconstruction error of the initialisation
%
%----------------------------
% Credits to Jeremy Cohen. This function is designed for educational
% purpose. Made for INSA Dictionary Learning course.
%--------------------------------------------------------------------------
%%

% Parameters
[m,n] = size(Y);

%% Initialisation of the dictionary
    % Dictionary sizes
di = fact*dims;
    % Generating the DCT matrices
D1 = dctmtx(di(1)); 
D2 = dctmtx(di(2));
D3 = dctmtx(di(3));
    % Truncating the DCT matrices
D1 = D1(1:dims(1),:);
D2 = D2(1:dims(2),:);
D3 = D3(1:dims(3),:);
    % Normalizing after truncation
D1 = D1.*repmat(1./sqrt(sum(D1.^2)),dims(1),1);
D2 = D2.*repmat(1./sqrt(sum(D2.^2)),dims(2),1);
D3 = D3.*repmat(1./sqrt(sum(D3.^2)),dims(3),1);
    % Creating the big dictionary (already normalized)
Do = kron(kron(D3,D2),D1);


%% Initialisation of the coefficients

% Printing stuff
fprintf('The DL algorithm is computing the initial coefficients for a DCT dictionary\n')

% Computation using OMP
[Xo,ei] = OMP(Y,Do,k);
fprintf('Coefficients have been initialized\n')
% Computing relative error
ei = ei/norm(Y,'fro')^2;


%% The MOD algorithm

% Parameters
itermax = 10;
d = fact^3*m;
l=1;
normY = norm(Y,'fro')^2;
ths = 0.05*ei;

% Variable initialisation
X = Xo;
D = Do;
e = ei;

% Printing
fprintf('Starting the main estimation loops\n')

while l<itermax && e > ths
   
   % Printing
   fprintf('%d, ',l)
   % error printing
   %if mod(l,10)==1
    fprintf(' \n error %d\n',e)
   %end
   
   % There might be zero rows in X; then any value in D is a minimizer. We
   % use the old one
       % Finding zero rows in X
   index = [];
   for p = 1:d
        if sum(X(p,:) ~= zeros(1,n)) ~= 0 
            index = [index,p];
        end
   end
       % D estimation as a least squares problem
   D(:,index) = Y/X(index,:);
   % D normalisation with safety guard
   norms = sqrt(sum(D.^2));
   norms(norms<10^-8) = 1;
   D = D.*repmat(1./norms,m,1);
   
   % X estimation using OMP
   [X,e] = OMP(Y,D,k);
   % Computing relative error
   e = e/normY;
   % Sanity check
   if ei < e
       fprintf('\n ------ Error is increasing ! ---------\n try other parameters')
       return
   end
   
   % Incrementing loop counter
   l = l+1;
   
end
    
end
