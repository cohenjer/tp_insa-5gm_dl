function [x,e] = OMP (y,D,k)
% [x,e] = OMP(y,D,k)
% This function computes a k-sparse coefficient vector x based on the
% Orthogonal Matching Pursuit Algorithm.
%------------
% inputs:
% 
% y : data vector (can be a matrix, then the OMP is applied to all columns
% of y independently)
% D : Dictionary matrix, normalized columnwise
% k : sparsity level of x
%------------
% outputs:
%
% x = sparse coefficient vector estimated from y.
% e = final reconstruction error
%------------
% Credits to Jeremy Cohen, made for INSA Dictionary Learning course.
%--------------------------------------------------------------------------

% Input caracteristics
[m,d] = size(D);
n = size(y,2);

% Initialisation of the residuals
res = y;

% Initialisation of coefficients x
x = zeros(d,n);

% Initialisation of the table containing the identified indexes
indexes = [];

for l=1:k
    % Printing current state every 10 steps
    if mod(l,10)==1
        fprintf('step %d out of %g\n',l,k)
    end
    
    % Computing the cross products to find the maximum of correlation
    crossp = D'*res;
    % Finding the maximum correlation index
    [~,ind] = max(abs(crossp));
    % Update table of indexes
    indexes = [indexes;ind];
    % Update of x using a least squares update 
    % sol = argmin_x || y - Ds x||_2^2 for each support (has to be looped)
    for i=1:n
        sol = D(:,indexes(:,i))\y(:,i); % Can be upgraded by computing Cholesky online
        x(indexes(:,i),i) = sol;
        % Update of residuals (everything must be recomputed since all x is updated)
        res(:,i) = y(:,i) - D(:,indexes(:,i))*x(indexes(:,i),i);
    end

end

e = norm(y - D*x,'fro')^2;

end
