%% Test of DLmod

%% Testing the initialisation
clear variables
close all
clc

% Importing a test image
Edouard = imread('faces_gouv/Edouard.jpg');

% Parameters
k = 35;
dims = [15,15,3];
skip = 5;
fact = 1;

% Building a matrix of data patches
Y = patches(Edouard,dims,skip);

%% Testing the function
    % Sparse coding steps
[Ddl,Xdl,edl,Ddct,Xdct,ei] =  DLmod (Y,k,dims,fact);

    % initialisation test
    % Reconstructed patches
Ydct = Ddct*Xdct;
    % Reconstructed image
Edouarddct = patch2image(Ydct,dims,skip,[150,150,3]);    


    % Learnt dico test
    % Reconstructed patches
Ydl = Ddl*Xdl;
    % Reconstructed image
Edouarddl = patch2image(Ydl,dims,skip,[150,150,3]);    
    % Showing reconstructed Edouard and real one
        % Showing reconstructed Edouard and real one
figure
imshow(Edouard)
title('True Edouard')

figure
imshow(Edouarddct)    
title('Edouard DCT')

figure
imshow(Edouarddl)  
title('Edouard Dictionary Learning')

