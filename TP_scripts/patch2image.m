function myImage = patch2image (Y, dims, skip, imdim)
%% Help of patch2image 
% image = patch2image (Y, dimensions, skip, imdim)
% This function computes an image from a patch matrix Y, using the average of
% the values of the same pixel when they belong to several patches.
%-----------------
% inputs:
%
% Y : a matrix containing the patches. Cannot have missing data. Must be in
% double format.
% dims : dimensions of the patches. Example: [10,10,3]. Third dimensions has to
% be 3 (this function is designed for RGB images or masks of missing data for
% RGB images).
% skip : ammount of shift when building the patches
% imdim : dimensions of the image. Example: [150,150,3].
%
% outputs:
%
% image : tensor containing RGB intensities between 1 and 255. Pixel values are
% averaged over shared patches and floored.
%
%----------------
% Credits to Jeremy Cohen. This function is designed for educational
% purpose. Made for INSA Dictionary Learning course.
%---------------------------------------------------------------------------

% Sanity check
if dims(3) ~= 3
    fprintf('dims(3) has to be 3')
    return
end

% Initialisation of the image
myImage = zeros(imdim);
meancount = zeros(imdim);

% Initialisation of counters
x = 1;
y = 1;
p = 1;

% Building the image with superpositions
while x+dims(1)-1<=imdim(1)
    while y+dims(2)-1<=imdim(2)              
            % Fetching the patch
        impatch = reshape(Y(:,p),dims);
            % adding the patch to the image
        myImage(x:(x+dims(1)-1),y:(y+dims(2)-1),1:dims(3)) = ...
        myImage(x:(x+dims(1)-1),y:(y+dims(2)-1),1:dims(3)) + impatch;
            % counting how many times each pixel has been written on
        meancount(x:(x+dims(1)-1),y:(y+dims(2)-1),1:dims(3)) = ...
        meancount(x:(x+dims(1)-1),y:(y+dims(2)-1),1:dims(3)) + 1;
            % counters
        y = y + skip;
        p = p+1;
    end
    x = x + skip;
    y = 1;
end

% Averaging and rounding 
myImage = uint8(round(myImage./meancount));
