function [x,e] = OMP_missing (y,D,k,mask)
% x = OMP(y,D,k)
% This function computes a k-sparse coefficient vector x based on the
% Orthogonal Matching Pursuit Algorithm. A mask of missing data can be
% specified on y (or each column of y if it is a matrix).
%------------
% inputs:
% 
% y : data vector (can be a matrix, then the OMP is applied to all columns
% of y independently)
% D : Dictionary matrix, normalized columnwise
% k : sparsity level of x
% mask : a binary vector (matrix) of same size as y, stating which entries of y
% are unknown (0 for unknown, 1 for known).
%------------
% outputs:
%
% x = sparse coefficient vector estimated from y.
% e = final reconstruction error.
%------------
% Credits to Jeremy Cohen, made for INSA Dictionary Learning course.
%--------------------------------------------------------------------------

% Input caracteristics
[m,d] = size(D);
n = size(y,2);

% Initialisation of the residuals
res = y;

% Initialisation of the cross products
crossp = zeros(d,n);

% Initialisation of coefficients x
x = zeros(d,n);

% Initialisation of the table containing the identified indexes
indexes = [];

for l=1:k
    % Printing current state
    fprintf('step %d out of %g\n',l,k)
    
    % Computing the cross products to find the maximum of correlation
    for i=1:n
        crossp(:,i) = D(mask(:,i),:)'*res(mask(:,i),i);
    end
    % Finding the maximum correlation index
    [~,ind] = max(abs(crossp));
    % Update table of indexes
    indexes = [indexes;ind];
    % Update of x using a least squares update 
    % sol = argmin_x || y - Ds x||_2^2 for each support (has to be looped)
    for i=1:n
        sol = D(mask(:,i),indexes(:,i))\y(mask(:,i),i);
        x(indexes(:,i),i) = sol;
        % Update of residuals (everything must be recomputed since all x is updated)
        % rq: no need for mask, since those entries are not used anyways
        res(:,i) = y(:,i) - D(:,indexes(:,i))*x(indexes(:,i),i);
    end

end

e = norm(y - D*x,'fro')^2;

end