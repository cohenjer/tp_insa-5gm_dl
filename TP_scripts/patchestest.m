%% Testing the Patches and rebuild function

clear variables
close all
clc

% Importing a test image
Edouard = imread('faces_gouv/Edouard.jpg');

% Parameters
dims = [10,10,3];
skip = 5;

% Building a matrix of data patches
Y = patches(Edouard,dims,skip);

% Showing some patches
p1 = uint8(Y(:,50)); imshow(reshape(p1,dims(1),dims(2),3))

% Rebuilding the image from the overlapping patches
Edouard2 = patch2image(Y,dims,skip,size(Edouard));

% Showing the rebuilt image (should be identical to the original image)
figure; imshow(Edouard2)
